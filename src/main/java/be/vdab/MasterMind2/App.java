package be.vdab.MasterMind2;

import static be.vdab.utility.InputHandler.InputIntHandler;
import static be.vdab.utility.InputHandler.inputStrHandler;
/**
 *This is the main class for Master mind guessing game
 * @author Mehmet Durmus
 *
 *
 * */
public class App {
    MasterMind2 mastermind2 = new MasterMind2();

/**
 * Main method that call methods for starting the game
 * */
    public static void main(String[] args) {
        App app = new App();

        app.startGame();

    }

    /**
     * Starting instrunctions printing and calls the run game method
     */
    public void startGame(){
        System.out.println("" +
                "Welcome to Mastermind!\n" +
                "----------------------\n" +
                "I'm thinking of a 4 digit code. numbers starting from 1 up to 6, duplicates not allowed\n" +
                "+ represents a correct number guessed in the correct position - represents a correct number guessed in the wrong posisiton");


        runGame();

    }

    /**
     * runGame method for the game loop checks answers and guess are okay then asking question to user to continue or not.
     * According to user's choice the game is ended or make a new fresh round.
     */
    void runGame(){
        while (!mastermind2.isFinishedGame()){ // if finished game turned to true than while loop will finish and turn back to startGame
            System.out.println("Make your guess: ");
            int tmp = InputIntHandler();
            if(tmp!=-1){
                mastermind2.setGuess(String.valueOf(tmp)); // getting the input as guess and store it as String into Answer field
                System.out.println("Your Guess: " + mastermind2.getGuess());
                String checkedAnswer = mastermind2.checkGuess(mastermind2.getGuess()); //Checking the guess

                System.out.println(mastermind2.getAnswer());
                if(checkedAnswer.equals(MasterMind2.CORRECT_GUESS)){
                    System.out.println("Answer: " + MasterMind2.CORRECT_GUESS);
                    System.out.println("Congratulations! You passed with in " + mastermind2.getCountGuesses() + " tries.");
                    System.out.println("Do you want to play again(y/n empty = n): ");

                    if(inputStrHandler().equals("y")){
                        mastermind2.reset();
                    }else{
                        mastermind2.setFinishedGame(true);
                    }
                }else{
                    System.out.println("Answer: "+checkedAnswer);
                }
            }






     }
    }

}
