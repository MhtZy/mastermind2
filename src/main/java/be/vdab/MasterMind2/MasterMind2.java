package be.vdab.MasterMind2;

import java.util.Random;




/**
 * Master mind Guessing Game Class
 *
 * @author Mehmet Durmus
 *
 *
 * */
public class MasterMind2 {
    /**
     * Variables section some of them final and static, others are private with initial value;
     */
    static final String FULLVALID_GUESS = "+";
    static final  String SEMIVALID_GUESS = "-";
    static final String CORRECT_GUESS = "++++";

    private int countGuesses=0;
    private String guess="";
    private String answer="";
    private boolean finishedGame=false;

    /**
     * Our constructor which is each time calls the creataRandomAnswer to fill the answer variable randomly.
     */
    public MasterMind2(){
        createRandomAnswer();

    }

    /**
     * To fill the answer variable with randomly in range of 1-6 , non-duplicate and 4 length long.
     */
    private void createRandomAnswer(){
        Random rnd = new Random();

        while(getAnswer().length()<4){
            String tmp = String.valueOf(rnd.nextInt(6)+1);

            if(!getAnswer().contains(tmp)){
              setAnswer(getAnswer()+tmp);
          }
        }


    }

    /**
     * This method compare with
     * @param guess
     * and the Actual answer variable,which filled before in createRandomAnswer method then
     * @return result string that "+,-" includes according to comparision.
     *
     */
    public String checkGuess(String guess){

        if(guess == null||guess.isEmpty()||guess.isBlank()){
            throw new IllegalArgumentException("Please, give proper parameter!");
        }

        String result ="";
        for (int i = 0; i < getAnswer().length();i++){
            for (int j = 0 ; j < getGuess().length();j++){
                if(i==j && answer.charAt(i)==guess.charAt(i)){
                    result= new String(result + FULLVALID_GUESS);

                }else if(i!=j && answer.charAt(i)==guess.charAt(j)){
                    result= new String(result+SEMIVALID_GUESS);
                }
            }

        }

        this.countGuesses++;
        return result;
    }

    /**
     * reset method using for when player wants to play again resetting every variables to default values.
     */
    public void reset(){

        setCountGuesses(0);
        setFinishedGame(false);
        this.answer="";
        this.guess="";
        createRandomAnswer();

    }




    /* *********** Getters and Setters ***********
    */

    public String getGuess() {
        return this.guess;
    }

    public int getCountGuesses() {
        return this.countGuesses;
    }

    public String getAnswer() {
        return this.answer;
    }

    public boolean isFinishedGame() {
        return this.finishedGame;
    }

    /**
     * With validation of setting Guess variable
     * @param guess setting the Guess variable according this parameter.
     */
    public void setGuess(String guess) {
        if(guess == null|| guess.isBlank()) {
            throw new IllegalArgumentException("This object could not be null");
        }
        this.guess = guess;
    }

    /**
     * with validation setting counter variable for how many times proper guess entered.
     * @param countGuesses setting the countGuess variable according the parameter.
     */
    public void setCountGuesses(int countGuesses) {
        if (countGuesses <0){
            throw new IllegalArgumentException("Count could not be miner than zero!");
        }
        this.countGuesses = countGuesses;
    }

    /**
     * setting answer with validation
     * @param answer for setting the answer acording our wants.
     */
    public void setAnswer(String answer) {
        if(answer == null|| answer.isBlank()) {
            throw new IllegalArgumentException("This object could not be null");
        }
        this.answer = answer;
    }

    public void setFinishedGame(boolean finishedGame) {

        this.finishedGame = finishedGame;
    }
}
