package be.vdab.utility;
import java.util.Scanner;

/**
 * @author Mehmet Durmus
 *
 * This class for handiling the Integer and String inputs and checks them.
 */
public class InputHandler {


    // Checking if next input is an integer or not, if it is not than catching the Exceptions

    /**
     * Cheking integer input for guess and comparing to answer if wrong structure
     * @return -1 or if it is proper entry then return integer value of the input
     */
    public static int InputIntHandler() {
        Scanner keyboard = new Scanner(System.in);

            try {
                String testIn = keyboard.nextLine();
                if(testIn.length() == 4 ){

                    for (int i =0; i < testIn.length();i++){
                        for(int j = 1; j < testIn.length(); j++){
                            if( i!=j && testIn.charAt(i) == testIn.charAt(j)|| testIn.charAt(i)>'6' || testIn.charAt(i)<'1'){

                                System.out.println("Please, enter non-duplicate and range in 1-6 input");
                                return -1;
                            }
                        }
                    }
                    return Integer.parseInt(testIn);

                }
                else {
                    System.out.println("Please, enter only 4 digit number");
                    return -1;
                }
            } catch (Exception e) {
                System.out.println("input is not proper,please enter again");
                return -1;
            }


    }

    /**
     * Checking String inputs according to what wants from user and looping until valid input entered.
     * @return only "n" or "y"
     */
    public static String inputStrHandler(){
        Scanner keyboard = new Scanner(System.in);
        while (true) {

            String inputStr = keyboard.nextLine();
                try {

                    if (inputStr != null && !inputStr.equals("") && inputStr.toLowerCase().equals("y")) {

                        return inputStr.toLowerCase();
                    } else if (inputStr.isEmpty() || inputStr.toLowerCase().equals("n") || inputStr.isBlank()) {
                        inputStr = "n";
                        return inputStr;
                    } else {
                        throw new Exception("please enter only 'y' or 'n': ");

                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());

                }

        }
    }
}

