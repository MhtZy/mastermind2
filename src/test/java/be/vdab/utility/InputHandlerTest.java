package be.vdab.utility;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

/**
 * @author Mehmet Durmus
 * Testing Class for the inputHandler class
 */
class InputHandlerTest {


    /**
     * Testing according giving condition in arrays in for loops and outcomes
     * invalid and valid input values are stored as two different array and checking them separately.
     */
    @Test
    void inputIntHandler() {

        String[] testBoundariesItems = {"1111"," ","","\n","6789","0123","123","1","-1","-1234","abcd","12345678","12","0"};
        String[] testProperItems = {"1234","2345","1456","3564"};

        for (String s : testBoundariesItems){
            InputStream in = new ByteArrayInputStream(s.getBytes());
            System.setIn(in);
            Assertions.assertEquals(-1, InputHandler.InputIntHandler());
        }
        for (String s : testProperItems){
            InputStream in = new ByteArrayInputStream(s.getBytes());
            System.setIn(in);
            Assertions.assertEquals(Integer.parseInt(s), InputHandler.InputIntHandler());
        }


    }

    /**
     * Testing according giving condition in arrays in for loops and outcomes
     * invalid and valid input values are stored as three different array and checking them separately.
     */
    @Test
    void inputStrHandler() {
        String[] testWrongItems = {"asd","3564","1","0","-1",""};
        String[] testProperItems = {" ","n","N","\n"};
        String[] testYesItems = {"y","Y"};



        for (String s : testProperItems){
            InputStream in2 = new ByteArrayInputStream(s.getBytes());
            System.setIn(in2);
            Assertions.assertEquals("n", InputHandler.inputStrHandler());
        }
        for (String s : testYesItems){
            InputStream in3 = new ByteArrayInputStream(s.getBytes());
            System.setIn(in3);
            Assertions.assertEquals("y", InputHandler.inputStrHandler());
        }
        for (String s : testWrongItems){
            InputStream in4 = new ByteArrayInputStream(s.getBytes());
            System.setIn(in4);
            Assertions.assertThrows(Exception.class, InputHandler::inputStrHandler);

        }
    }
}