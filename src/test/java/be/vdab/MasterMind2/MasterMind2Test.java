package be.vdab.MasterMind2;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

/**
 * This class for testing the Mastermind2 class methods.
 * @author Mehmet Durmus
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MasterMind2Test {
    MasterMind2 masterMind2 = new MasterMind2();

    /**
     * this is for testing the checkGuess method
     * if we call the checkGuess method with parameters are null,empty or blank or valid input. Tests those conditions.
     */
    @Test
    void checkGuess() {
        String[] wrongGuessEntry ={""," ",null};
        for(String s : wrongGuessEntry){
            Assertions.assertThrows(IllegalArgumentException.class, ()-> masterMind2.checkGuess(s));
        }
        masterMind2.setAnswer("1234");
        masterMind2.setGuess("6521");
        Assertions.assertEquals("--",masterMind2.checkGuess("6521"));

        masterMind2.setAnswer("1234");
        masterMind2.setGuess("1234");
        Assertions.assertEquals("++++",masterMind2.checkGuess("1234"));

    }

    /**
     * Testing the get method of Guess variable is it works properly or not.
     */
    @Test
    void getGuess() {
        masterMind2.setGuess("should be this ");
        Assertions.assertEquals("should be this ",masterMind2.getGuess());
    }
    /**
     * Testing the get method of countGuess variable is it works properly or not.
     */
    @Test
    void getCountGuesses() {
        masterMind2.setCountGuesses(28);
        Assertions.assertEquals(28,masterMind2.getCountGuesses());
    }
    /**
     * Testing the get method of answer variable is it works properly or not.
     */
    @Test
    void getAnswer() {
        masterMind2.setAnswer("Should be this");
        Assertions.assertEquals("Should be this",masterMind2.getAnswer());
    }
    /**
     * Testing the method of finishedGame variable is it works properly or not.
     */
    @Test
    void isFinishedGame() {
        masterMind2.setFinishedGame(true);
        Assertions.assertTrue(masterMind2.isFinishedGame());

        masterMind2.setFinishedGame(false);
        Assertions.assertFalse(masterMind2.isFinishedGame());

    }
    /**
     * Testing the set method of Guess variable is it works properly or not.
     */
    @Test
    void setGuess() {
        String[] wrongAnswers = {" ",""};
        for(String s : wrongAnswers){
            Assertions.assertThrows(IllegalArgumentException.class, ()-> masterMind2.setGuess(s));
        }
        masterMind2.setAnswer("normalString");
        Assertions.assertEquals("normalString",masterMind2.getAnswer());
    }
    /**
     * Testing the set method of countGuess variable is it works properly or not.
     */
    @Test
    void setCountGuesses() {
        int[] wrongAnswers = {-10,-6,-1};
        int[] properAnswers = {0,1,6,10};

        for(int s : wrongAnswers){
            Assertions.assertThrows(IllegalArgumentException.class, ()-> masterMind2.setCountGuesses(s));
        }

        for(int s : properAnswers){
            masterMind2.setCountGuesses(s);
            Assertions.assertEquals(s,masterMind2.getCountGuesses());
        }
    }
    /**
     * Testing the set method of answer variable is it works properly or not.
     */
    @Test
    void setAnswer() {
        String[] wrongAnswers = {" ","",null};
        for(String s : wrongAnswers){
            Assertions.assertThrows(IllegalArgumentException.class, ()-> masterMind2.setAnswer(s));
        }

        masterMind2.setAnswer("abc");
        Assertions.assertEquals("abc",masterMind2.getAnswer());
    }
    /**
     * Testing the set method of finishedGame variable is it works properly or not.
     */
    @Test
    void setFinishedGame() {
        masterMind2.setFinishedGame(true);
        Assertions.assertTrue(masterMind2.isFinishedGame());

        masterMind2.setFinishedGame(false);
        Assertions.assertFalse(masterMind2.isFinishedGame());
    }
}